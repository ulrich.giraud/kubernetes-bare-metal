DOCKER_REGISTRY_MIRROR=https://docker-dev-virtual.repository.pole-emploi.intra
QUAY_REGISTRY_MIRROR=docker-quay-proxy.repository.pole-emploi.intra
K8S_REGISTRY_MIRROR=docker-k8s-gcr-proxy.repository.pole-emploi.intra
INSECURE_REGISTRIES=["docker-dev-virtual.repository.pole-emploi.intra", "docker-quay-proxy.repository.pole-emploi.intra", "docker-k8s-gcr-proxy.repository.pole-emploi.intra"]
CENTOS_BASE_MIRROR=https://repository.pole-emploi.intra/artifactory/rpm-base-centos-proxy
DOCKER_YUM_MIRROR=https://repository.pole-emploi.intra/artifactory/rpm-docker-centos-proxy
K8S_YUM_MIRROR=https://repository.pole-emploi.intra/artifactory/rpm-kubernetes-proxy/
